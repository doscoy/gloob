# gloob

globのパチモン

## 使い方

```cpp
#include <gloob/gloob.hpp>

using entry = gloob::iterator::value_type;
static_assert(std::is_same_v<std::filesystem::directory_entry, entry>);

// 全てのpngファイルを走査
for (const entry& itr: gloob::iterator{"*.png"}) {
  std::cout << itr.path() << '\n';
}

// rootの指定
for (const entry& itr: gloob::iterator{"/path/to/root", "*.png"}) {
  std::cout << itr.path() << '\n';
}

// ソート
std::vector<entry> list{gloob::iterator{"frame*.gif"}, gloob::iterator{}};
std::sort(std::begin(list), std::end(list));

// std::filesystem::directory_iterator等と同じようなオプションを取ることができる
std::filesystem::directory_options opts{};
std::error_code ec{};
gloob::iterator itr{"/path/to/root/", "pattern", opts, ec};
```
## マッチパターン
|パターン|効果|
|:-:|:-|
|*|0文字以上の任意の文字にマッチ|
|?|任意の1文字にマッチ|
|\[abc\]|括弧内のどれか1文字にマッチ|
|\[0-9\]|括弧内の範囲1文字にマッチ|
|\[!abc\]|括弧内のどれか以外1文字にマッチ|
|\[!0-9\]|括弧内の範囲以外1文字にマッチ|
|**|ディレクトリを再帰的に探査する|

- マッチはUTF-32に変換された上で評価される
  - \[ぁ-ん\]でひらがな1文字にマッチする
- 末尾がパス区切り文字の場合はディレクトリ、それ以外の場合はファイルにマッチする
  - `**/*/`で全てのディレクトリにマッチする
  - `**/*`で全てのファイルにマッチする
