#include <algorithm>
#include <chrono>
#include <filesystem>
#include <gloob/gloob.hpp>
#include <iostream>

int main(int argn, char **argv) {
  namespace c = std::chrono;
  const auto path_name{argn > 1 ? argv[1] : "**/*"};
  const auto start_time{c::steady_clock::now()};

  std::cout << "path_name: " << path_name << '\n';
  int  cnt{0};
  for (const auto &itr : gloob::iterator{path_name}) {
    std::cout << itr.path().string() << '\n';
    ++cnt;
  }
  std::cout << cnt << " items\n";

  const auto end_time{c::steady_clock::now()};
  const auto dur{end_time - start_time};
  if (const auto s{c::duration_cast<c::seconds>(dur).count()}) {
    std::cout << s << " s\n";
  } else if (const auto ms{c::duration_cast<c::milliseconds>(dur).count()}) {
    std::cout << ms << " ms\n";
  } else if (const auto us{c::duration_cast<c::microseconds>(dur).count()}) {
    std::cout << us << " us\n";
  }

  return 0;
}
