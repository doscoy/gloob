#pragma once

#include <memory>
#include <string_view>

namespace gloob {

class Match {
public:
  class Checker {
  public:
    virtual ~Checker() {}
    virtual bool Check(std::u32string_view &src) const noexcept = 0;
    virtual bool Peek(std::u32string_view::value_type c) const noexcept = 0;
    template <class T, class... Args> Checker *Append(Args &&...args) {
      if (next_) {
        return next_->Append<T>(std::forward<Args>(args)...);
      } else {
        next_.reset(new T{std::forward<Args>(args)...});
        return next_.get();
      }
    }

  protected:
    std::unique_ptr<Checker> next_;
  };

  Match(const std::u32string_view &pattern);

  bool Check(const std::u32string_view &src) const noexcept {
    auto s{src};
    return checker_->Check(s);
  }

  bool Test(std::u32string_view &src) const noexcept {
    return checker_->Check(src);
  }

private:
  std::unique_ptr<Checker> checker_;
};

bool IsPattern(const std::u32string_view &str) noexcept;

} // namespace gloob
