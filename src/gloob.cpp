#include "digger.hpp"
#include <filesystem>
#include <gloob/gloob.hpp>
#include <memory>
#include <string_view>
#include <system_error>

namespace {

namespace fs = std::filesystem;

}

namespace gloob {

iterator::iterator(const fs::path &root_dir, const fs::path &path_name,
                   fs::directory_options options) {
  std::error_code ec{};
  digger_ = std::make_shared<Digger>(root_dir, path_name, options, ec);
  if (ec) {
    throw ec;
  }
  if (not digger_->Has()) {
    digger_.reset();
  }
}

iterator::iterator(const fs::path &root_dir, const fs::path &path_name,
                   fs::directory_options options, std::error_code &ec) {
  digger_ = std::make_shared<Digger>(root_dir, path_name, options, ec);
  if (not digger_->Has()) {
    digger_.reset();
  }
}

iterator::reference iterator::operator*() const noexcept {
  return digger_->GetEntry();
}

iterator &iterator::increment(std::error_code &ec) {
  digger_->Search(ec);
  if (not digger_->Has()) {
    digger_.reset();
  }
  return *this;
}

} // namespace gloob
