#include "match.hpp"
#include <memory>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

namespace {

std::u32string_view::size_type FindRBracket(const std::u32string_view &p) {
  if (p.front() != U'[') {
    return std::u32string_view::npos;
  }
  std::u32string_view::size_type pos{2};
  while (pos < p.size()) {
    pos = p.find(U']', pos);
    if (pos != std::u32string_view::npos) {
      if (p[pos - 1] == U'[') {
        ++pos;
      } else {
        return pos;
      }
    }
  }
  return std::u32string_view::npos;
}

} // namespace

namespace {

class JustStr final : public gloob::Match::Checker {
public:
  JustStr(const std::u32string_view &str) : str_{str} {}

  bool Check(std::u32string_view &src) const noexcept override {
    if (src.find(str_) == 0) {
      src.remove_prefix(str_.size());
      if (src.empty()) {
        return not next_;
      } else if (next_) {
        return next_->Check(src);
      }
    }
    return false;
  }

  bool Peek(std::u32string_view::value_type c) const noexcept override {
    return c == str_.front();
  }

private:
  std::u32string_view str_;
};

class WildCard final : public gloob::Match::Checker {
public:
  bool Check(std::u32string_view &src) const noexcept override {
    if (not next_) {
      return true;
    }
    while (not src.empty()) {
      if (next_->Peek(src.front())) {
        return next_->Check(src);
      }
      src.remove_prefix(1);
    }
    return false;
  }

  bool Peek(std::u32string_view::value_type) const noexcept override {
    return true;
  }
};

class CountWild final : public gloob::Match::Checker {
public:
  CountWild(std::u32string_view::size_type count) : count_{count} {}

  bool Check(std::u32string_view &src) const noexcept override {
    if (src.empty()) {
      return false;
    } else if (src.size() == count_) {
      src.remove_prefix(count_);
      return not next_;
    } else if (src.size() > count_) {
      src.remove_prefix(count_);
      if (next_) {
        return next_->Check(src);
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool Peek(std::u32string_view::value_type) const noexcept override {
    return count_ > 0;
  }

private:
  std::u32string_view::size_type count_;
};

class CharRange {
public:
  CharRange(std::u32string_view::value_type beg,
            std::u32string_view::value_type end)
      : beg_{beg < end ? beg : end}, end_{beg < end ? end : beg} {}

  bool Has(std::u32string_view::value_type c) const noexcept {
    return beg_ <= c && c <= end_;
  }

private:
  std::u32string_view::value_type beg_, end_;
};

class CharList {
public:
  CharList(const std::u32string_view &list) : list_{list} {}

  bool Has(std::u32string_view::value_type c) const noexcept {
    return list_.find(c) != std::u32string_view::npos;
  }

private:
  std::u32string_view list_;
};

class BracketsBase : public gloob::Match::Checker {
public:
  BracketsBase(const std::u32string_view &inside_brackets) {
    auto s{inside_brackets};
    if (s.front() == U'!' && s.size() != 1) {
      s.remove_prefix(1);
      negative_ = true;
    } else {
      negative_ = false;
    }

    std::u32string_view::size_type skip{0};
    while (not s.empty()) {
      const auto hpos{s.find(U'-', skip)};
      if (hpos == std::u32string_view::npos || hpos == s.size() - 1) {
        items_.emplace_back(CharList{s});
        return;
      } else if (hpos == 0) {
        skip = hpos + 1;
      } else {
        if (hpos > 1) {
          const auto list{s.substr(0, hpos - 1)};
          items_.emplace_back(CharList{list});
        }
        items_.emplace_back(CharRange{s[hpos - 1], s[hpos + 1]});
        s.remove_prefix(hpos + 2);
        skip = 0;
      }
    }
  }

  virtual ~BracketsBase() {}

  bool Peek(std::u32string_view::value_type c) const noexcept override final {
    const auto pred{[c](const auto &item) {
      return std::visit([c](const auto &item) { return item.Has(c); }, item);
    }};
    const auto has{std::any_of(std::begin(items_), std::end(items_), pred)};
    return has != negative_;
  }

private:
  std::vector<std::variant<CharList, CharRange>> items_;
  bool negative_;
};

class SingleBrackets final : public BracketsBase {
public:
  using BracketsBase::BracketsBase;
  bool Check(std::u32string_view &src) const noexcept override {
    if (not Peek(src.front())) {
      return false;
    }
    src.remove_prefix(1);
    if (src.empty()) {
      return not next_;
    }
    if (next_) {
      return next_->Check(src);
    }
    return false;
  }
};

class RepeatBrackets final : public BracketsBase {
public:
  using BracketsBase::BracketsBase;
  bool Check(std::u32string_view &src) const noexcept override {
    while (not src.empty()) {
      if (not Peek(src.front())) {
        if (next_) {
          return next_->Check(src);
        } else {
          return false;
        }
      }
      src.remove_prefix(1);
    }
    return not next_;
  }
};

class DummyChecker final : public gloob::Match::Checker {
public:
  DummyChecker(){};
  bool Check(std::u32string_view &) const noexcept override { return false; }
  bool Peek(std::u32string_view::value_type) const noexcept override final {
    return false;
  }
  decltype(next_) &&Pop() { return std::move(next_); }
};

} // namespace

namespace {

std::unique_ptr<gloob::Match::Checker>
ParsePattern(const std::u32string_view &pattern) {
  DummyChecker dummy{};
  gloob::Match::Checker *head{&dummy};
  std::u32string_view p{pattern};
  while (not p.empty()) {
    switch (p.front()) {
    case U'*': {
      const auto end{std::min(p.size(), p.find_first_not_of(U"*?"))};
      head = head->Append<WildCard>();
      p.remove_prefix(end);
      break;
    }
    case U'?': {
      const auto end{std::min(p.size(), p.find_first_not_of(U'?'))};
      head = head->Append<CountWild>(end);
      p.remove_prefix(end);
      break;
    }
    case U'[': {
      const auto end{FindRBracket(p)};
      if (end == std::u32string_view::npos) {
        break;
      }
      head = head->Append<SingleBrackets>(p.substr(1, end - 1));
      p.remove_prefix(end + 1);
    }
    }
    if (p.empty()) {
      break;
    }
    const auto next{p.find_first_of(U"*?[")};
    if (next != 0) {
      auto str{p.substr(0, next)};
      p.remove_prefix(str.size());
      head = head->Append<JustStr>(std::move(str));
    }
  }
  return dummy.Pop();
}

} // namespace

namespace gloob {

Match::Match(const std::u32string_view &pattern)
    : checker_(ParsePattern(pattern)) {
  if (not checker_) {
    checker_ = std::make_unique<DummyChecker>();
  }
}

bool IsPattern(const std::u32string_view &str) noexcept {
  const auto magic{str.find_first_of(U"*?[")};
  if (magic == std::u32string_view::npos) {
    return false;
  }
  switch (str[magic]) {
  case U'*':
  case U'?':
    return true;
  case U'[':
    return FindRBracket(str.substr(magic)) != std::u32string_view::npos;
  default:
    return false;
  }
}

} // namespace gloob
