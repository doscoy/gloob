#include "digger.hpp"

#include <filesystem>
#include <memory>
#include <string>
#include <system_error>

#include "match.hpp"

namespace {

namespace fs = std::filesystem;

}

namespace gloob {

bool Digger::Node::Search(std::error_code &ec) {
  if (not next_) {
    return SearchImpl(ec);
  }
  while (true) {
    if (next_->Search(ec)) {
      return true;
    } else if (SearchImpl(ec)) {
      next_->Init(GetEntryImpl(), ec);
      if (next_->Has()) {
        return true;
      }
    } else {
      return false;
    }
  }
}

}  // namespace gloob

namespace {

class JustPath final : public gloob::Digger::Node {
 public:
  JustPath(fs::path &&path) : path_{std::move(path)}, entry_{} {}

  void Init(const fs::directory_entry &cur, std::error_code &ec) override {
    entry_.assign((cur.path() / path_).lexically_normal(), ec);
    if (ec == std::errc::no_such_file_or_directory) {
      entry_ = fs::directory_entry{};
      ec.clear();
      return;
    }
    if (next_) {
      next_->Init(entry_, ec);
    }
  }

 protected:
  bool HasImpl() const noexcept override { return not entry_.path().empty(); }

  const fs::directory_entry &GetEntryImpl() const noexcept override {
    return entry_;
  }

  bool SearchImpl(std::error_code &) override {
    entry_ = fs::directory_entry{};
    return false;
  }

 private:
  fs::path path_;
  fs::directory_entry entry_;
};

class Pattern final : public gloob::Digger::Node {
 public:
  Pattern(std::u32string &&pattern, fs::directory_options opts)
      : pattern_{std::move(pattern)}, match_{pattern_}, opts_{opts}, itr_{} {}

  void Init(const fs::directory_entry &cur, std::error_code &ec) override {
    itr_ = fs::directory_iterator{cur.path(), opts_, ec};
    if (ec) {
      itr_ = fs::directory_iterator{};
      return;
    }

    is_first_ = true;
    if (not SearchImpl(ec) || ec) {
      itr_ = fs::directory_iterator{};
      return;
    }

    if (next_) {
      next_->Init(*itr_, ec);
      if (ec) {
        itr_ = fs::directory_iterator{};
        return;
      }
      while (not next_->Has()) {
        if (not SearchImpl(ec) || ec) {
          itr_ = fs::directory_iterator{};
          return;
        }
        next_->Init(*itr_, ec);
        if (ec) {
          itr_ = fs::directory_iterator{};
          return;
        }
      }
    }
  }

 protected:
  bool HasImpl() const noexcept override {
    return itr_ != fs::directory_iterator{};
  }

  const fs::directory_entry &GetEntryImpl() const noexcept override {
    return *itr_;
  }

  bool SearchImpl(std::error_code &ec) override {
    const fs::directory_iterator end{};
    if (is_first_) {
      is_first_ = false;
    } else if (itr_ != end) {
      itr_.increment(ec);
    }
    for (; itr_ != end && not ec; itr_.increment(ec)) {
      const auto path{itr_->path()};
      const auto name{path.has_filename() ? path.filename()
                                          : path.parent_path().filename()};
      if (match_.Check(name.u32string())) {
        return true;
      }
    }
    return false;
  }

 private:
  std::u32string pattern_;
  gloob::Match match_;
  fs::directory_options opts_;
  fs::directory_iterator itr_;
  bool is_first_;
};

class Recursive final : public gloob::Digger::Node {
 public:
  Recursive(fs::directory_options opts) : opts_{opts}, itr_{} {}

  void Init(const fs::directory_entry &cur, std::error_code &ec) override {
    itr_ = fs::recursive_directory_iterator{cur, opts_, ec};
    if (ec || not next_) {
      itr_ = fs::recursive_directory_iterator{};
      return;
    }

    next_->Init(cur, ec);
    if (ec) {
      itr_ = fs::recursive_directory_iterator{};
      return;
    }

    is_first_ = true;
    while (not next_->Has()) {
      if (not SearchImpl(ec) || ec) {
        itr_ = fs::recursive_directory_iterator{};
        return;
      }
      next_->Init(*itr_, ec);
      if (ec) {
        itr_ = fs::recursive_directory_iterator{};
        return;
      }
    }
  }

 protected:
  bool HasImpl() const noexcept override {
    return itr_ != fs::recursive_directory_iterator{};
  }

  const fs::directory_entry &GetEntryImpl() const noexcept override {
    return *itr_;
  }

  bool SearchImpl(std::error_code &ec) override {
    const fs::recursive_directory_iterator end{};
    if (is_first_) {
      is_first_ = false;
    } else if (itr_ != end) {
      itr_.increment(ec);
    }
    for (; itr_ != end && not ec; itr_.increment(ec)) {
      if (itr_->is_directory(ec)) {
        if (ec) {
          break;
        }
        return true;
      }
    }
    return false;
  }

 private:
  fs::directory_options opts_;
  fs::recursive_directory_iterator itr_;
  bool is_first_;
};

class TypeGuard final : public gloob::Digger::Node {
 public:
  TypeGuard(bool dir_only) : dir_only_{dir_only}, entry_{nullptr} {}
  void Init(const fs::directory_entry &cur, std::error_code &ec) override {
    if (next_) {
      entry_ = nullptr;
      return;
    }
    if (dir_only_) {
      if (cur.is_directory(ec) && not ec) {
        entry_ = &cur;
        return;
      }
    } else {
      if (not cur.is_directory(ec) &&
          (not ec || ec == std::errc::is_a_directory)) {
        entry_ = &cur;
        ec.clear();
        return;
      }
    }
    entry_ = nullptr;
  }

 protected:
  bool HasImpl() const noexcept override { return entry_; }

  const fs::directory_entry &GetEntryImpl() const noexcept override {
    return *entry_;
  }

  bool SearchImpl(std::error_code &) override {
    entry_ = nullptr;
    return false;
  }

 private:
  bool dir_only_;
  const fs::directory_entry *entry_;
};

class DummyNode final : public gloob::Digger::Node {
 public:
  DummyNode() : entry_{} {}
  void Init(const fs::directory_entry &, std::error_code &) override {}
  std::unique_ptr<Node> &&Pop() { return std::move(next_); }

 protected:
  bool HasImpl() const noexcept override { return false; }
  const fs::directory_entry &GetEntryImpl() const noexcept override {
    return entry_;
  }
  bool SearchImpl(std::error_code &) override { return false; }

 private:
  fs::directory_entry entry_;
};

}  // namespace

namespace {

std::unique_ptr<gloob::Digger::Node> ParsePathName(const fs::path &path_name,
                                                   fs::directory_options opts) {
  DummyNode dummy;
  gloob::Digger::Node *head{&dummy};
  fs::path buf{};
  auto add_just_path{[&head, &buf]() {
    if (not buf.empty()) {
      head = head->Append<JustPath>(std::move(buf));
      buf.clear();
    }
  }};

  bool dir_only;
  for (const auto &itr : path_name) {
    dir_only = false;
    auto u32{itr.generic_u32string()};
    if (gloob::IsPattern(u32)) {
      add_just_path();
      if (u32 == U"**") {
        head = head->Append<Recursive>(opts);
      } else {
        head = head->Append<Pattern>(std::move(u32), opts);
      }
    } else if (u32.empty()) {
      dir_only = true;
    } else {
      buf /= itr;
    }
  }
  add_just_path();
  head->Append<TypeGuard>(dir_only);
  return dummy.Pop();
}

}  // namespace

namespace gloob {

Digger::Digger(const fs::path &root, const fs::path &path_name,
               fs::directory_options opts, std::error_code &ec)
    : node_{ParsePathName(path_name, opts)} {
  if (node_) {
    node_->Init(fs::directory_entry{root}, ec);
  } else {
    node_ = std::make_unique<DummyNode>();
  }
}

}  // namespace gloob
