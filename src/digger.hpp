#pragma once
#include <filesystem>
#include <memory>
#include <system_error>

namespace gloob {

class Digger {
public:
  class Node {
  public:
    virtual ~Node() {}

    virtual void Init(const std::filesystem::directory_entry &cur,
                      std::error_code &ec) = 0;

    bool Has() const noexcept { return next_ ? next_->Has() : HasImpl(); }

    const std::filesystem::directory_entry &GetEntry() const noexcept {
      return next_ ? next_->GetEntry() : GetEntryImpl();
    }

    bool Search(std::error_code &ec);

    template <class T, class... Args> Node *Append(Args &&...args) {
      if (next_) {
        return next_->Append<T>(std::forward<Args>(args)...);
      } else {
        next_.reset(new T{std::forward<Args>(args)...});
        return next_.get();
      }
    }

  protected:
    virtual bool HasImpl() const noexcept = 0;

    virtual const std::filesystem::directory_entry &
    GetEntryImpl() const noexcept = 0;

    virtual bool SearchImpl(std::error_code &ec) = 0;

    std::unique_ptr<Node> next_;
  };

  Digger(const std::filesystem::path &root,
         const std::filesystem::path &path_name,
         std::filesystem::directory_options opts, std::error_code &ec);

  bool Has() const noexcept { return node_->Has(); }

  const std::filesystem::directory_entry &GetEntry() const noexcept {
    return node_->GetEntry();
  }

  void Search(std::error_code &ec) { node_->Search(ec); }

private:
  std::unique_ptr<Node> node_;
};

} // namespace gloob
