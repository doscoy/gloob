cmake_minimum_required(VERSION 3.11.0)
project(gloob VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if(CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
  set(IS_STANDALONE ON)
else()
  set(IS_STANDALONE OFF)
endif()

option(BUILD_SAMPLE "Build the sample program" ${IS_STANDALONE})
option(BUILD_TEST "Builds test" ${IS_STANDALONE})

add_subdirectory(src)

if(BUILD_TEST)
enable_testing()
add_subdirectory(test)
endif()