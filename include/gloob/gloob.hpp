#pragma once
#include <filesystem>
#include <iterator>
#include <memory>
#include <system_error>

namespace gloob {

class iterator {
public:
  using iterator_category = std::input_iterator_tag;
  using value_type = std::filesystem::directory_entry;
  using pointer = const value_type *;
  using reference = const value_type &;
  using difference_type = std::ptrdiff_t;

  iterator() noexcept = default;

  iterator(const std::filesystem::path &root_dir,
           const std::filesystem::path &path_name,
           std::filesystem::directory_options options);

  iterator(const std::filesystem::path &root_dir,
           const std::filesystem::path &path_name,
           std::filesystem::directory_options options, std::error_code &ec);

  iterator(const std::filesystem::path &root_dir,
           const std::filesystem::path &path_name)
      : iterator{root_dir, path_name,
                 std::filesystem::directory_options::none} {}

  iterator(const std::filesystem::path &root_dir,
           const std::filesystem::path &path_name, std::error_code &ec)
      : iterator{root_dir, path_name, std::filesystem::directory_options::none,
                 ec} {}

  iterator(const std::filesystem::path &path_name,
           std::filesystem::directory_options options)
      : iterator{std::filesystem::current_path(), path_name, options} {}

  iterator(const std::filesystem::path &path_name,
           std::filesystem::directory_options options, std::error_code &ec)
      : iterator{std::filesystem::current_path(ec), path_name, options, ec} {}

  iterator(const std::filesystem::path &path_name)
      : iterator{path_name, std::filesystem::directory_options::none} {}

  iterator(const std::filesystem::path &path_name, std::error_code &ec)
      : iterator{path_name, std::filesystem::directory_options::none, ec} {}

  iterator(const iterator &rhs) = default;
  iterator &operator=(const iterator &rhs) = default;

  iterator(iterator &&rhs) noexcept = default;
  iterator &operator=(iterator &&rhs) noexcept = default;

  reference operator*() const noexcept;
  pointer operator->() const noexcept { return &**this; }

  iterator &increment(std::error_code &ec);
  iterator &operator++() {
    std::error_code ec{};
    increment(ec);
    if (ec) {
      throw ec;
    }
    return *this;
  }

  friend bool operator==(const iterator &lhs, const iterator &rhs) noexcept {
    return not(rhs.digger_.owner_before(lhs.digger_)) &&
           not(lhs.digger_.owner_before(rhs.digger_));
  }

  friend bool operator!=(const iterator &lhs, const iterator &rhs) noexcept {
    return not(lhs == rhs);
  }

private:
  std::shared_ptr<class Digger> digger_;
};

inline iterator begin(iterator itr) noexcept { return itr; }
inline iterator end(iterator) noexcept { return iterator{}; }

} // namespace gloob
