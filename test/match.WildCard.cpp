#include <gtest/gtest.h>
#include <match.cpp>

TEST(WildCard, check) {
  WildCard a;
  {
    std::u32string_view b{U""};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"abcdアイウエオ"};
    ASSERT_TRUE(a.Check(b));
  }
  a.Append<JustStr>(U"a");
  {
    std::u32string_view b{U""};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"abcdアイウエオ"};
    ASSERT_FALSE(a.Check(b));
    ASSERT_EQ(b, U"bcdアイウエオ");
  }
  {
    std::u32string_view b{U"アイウエオabcd"};
    ASSERT_FALSE(a.Check(b));
    ASSERT_EQ(b, U"bcd");
  }
}