#include <gtest/gtest.h>

#include <match.cpp>

TEST(Match, empty) {
  const gloob::Match a{U""};
  ASSERT_FALSE(a.Check(U""));
  ASSERT_FALSE(a.Check(U"abc"));
}

TEST(Match, js_wc_js) {
  const gloob::Match a{U"file*.txt"};
  ASSERT_TRUE(a.Check(U"file_.txt"));
  ASSERT_TRUE(a.Check(U"file_乙.txt"));
  ASSERT_TRUE(a.Check(U"file_elif.txt"));
  {
    std::u32string_view b{U"xfile.txt"};
    ASSERT_FALSE(a.Test(b));
    ASSERT_EQ(b, U"xfile.txt");
  }
  {
    std::u32string_view b{U"file.bin"};
    ASSERT_FALSE(a.Test(b));
    ASSERT_EQ(b, U".bin");
  }
}

TEST(Match, js_cw_js) {
  const gloob::Match a{U"file_???.txt"};
  ASSERT_TRUE(a.Check(U"file_001.txt"));
  ASSERT_TRUE(a.Check(U"file_甲乙丙.txt"));
  {
    std::u32string_view b{U"file_01.txt"};
    ASSERT_FALSE(a.Test(b));
    ASSERT_EQ(b, U"txt");
  }
  {
    std::u32string_view b{U"file_0001.txt"};
    ASSERT_FALSE(a.Test(b));
    ASSERT_EQ(b, U"1.txt");
  }
}

TEST(Match, js_br_br_js) {
  const gloob::Match a{U"file_[あ-ん][!あ-ん].txt"};
  ASSERT_TRUE(a.Check(U"file_うス.txt"));
  {
    std::u32string_view b{U"file_うい.txt"};
    ASSERT_FALSE(a.Test(b));
    ASSERT_EQ(b, U"い.txt");
  }
  {
    std::u32string_view b{U"file_えン乙.txt"};
    ASSERT_FALSE(a.Test(b));
    ASSERT_EQ(b, U"乙.txt");
  }
}

TEST(Match, js_wc_br_js) {
  const gloob::Match a{U"file_*[a-z].txt"};
  ASSERT_TRUE(a.Check(U"file_ABCx.txt"));
  ASSERT_TRUE(a.Check(U"file_x.txt"));
  {
    std::u32string_view b{U"file_xx.txt"};
    ASSERT_FALSE(a.Test(b));
    ASSERT_EQ(b, U"x.txt");
  }
}