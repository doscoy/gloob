#include <gtest/gtest.h>

#include <digger.cpp>

const fs::path ROOT{TESTDIG_ROOT_DIR};

TEST(Pattern, check) {
  Pattern a{U"[!a]", fs::directory_options::none};
  std::error_code ec;
  std::vector<fs::directory_entry> buf;
  a.Init(fs::directory_entry{ROOT}, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  buf.push_back(a.GetEntry());
  ASSERT_TRUE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  buf.push_back(a.GetEntry());
  ASSERT_FALSE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  std::sort(std::begin(buf), std::end(buf));
  ASSERT_EQ(ROOT / "b", buf[0].path());
  ASSERT_EQ(ROOT / "c", buf[1].path());
}
