#include <gtest/gtest.h>
#include <match.cpp>

TEST(IsPattern, check) {
  ASSERT_TRUE(gloob::IsPattern(U"*"));
  ASSERT_TRUE(gloob::IsPattern(U"?"));
  ASSERT_TRUE(gloob::IsPattern(U"[a]"));
  ASSERT_FALSE(gloob::IsPattern(U"[]"));
}
