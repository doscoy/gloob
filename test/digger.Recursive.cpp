#include <gtest/gtest.h>

#include <digger.cpp>

const fs::path ROOT{TESTDIG_ROOT_DIR};

TEST(Recursive, fail_at_the_end) {
  Recursive a{fs::directory_options::none};
  std::error_code ec;
  a.Init(fs::directory_entry{ROOT}, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_FALSE(a.Has());
}

TEST(Recursive, check) {
  Recursive a{fs::directory_options::none};
  a.Append<Pattern>(U"3", fs::directory_options::none);
  std::error_code ec;
  std::vector<fs::directory_entry> buf;
  a.Init(fs::directory_entry{ROOT}, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  buf.push_back(a.GetEntry());
  ASSERT_TRUE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  buf.push_back(a.GetEntry());
  ASSERT_TRUE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  buf.push_back(a.GetEntry());
  ASSERT_FALSE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  std::sort(std::begin(buf), std::end(buf));
  ASSERT_EQ(ROOT / "a/3", buf[0].path());
  ASSERT_EQ(ROOT / "b/3", buf[1].path());
  ASSERT_EQ(ROOT / "c/3", buf[2].path());
}