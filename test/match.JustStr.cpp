#include <gtest/gtest.h>
#include <match.cpp>

TEST(match, check) {
  {
    const JustStr a{U""};
    std::u32string_view b{U""};
    ASSERT_TRUE(a.Check(b));
  }
  {
    const JustStr a{U"abc"};
    std::u32string_view b{U"abc"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    const JustStr a{U"アイウ"};
    std::u32string_view b{U"アイウ"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    const JustStr a{U"abc"};
    std::u32string_view b{U"def"};
    ASSERT_FALSE(a.Check(b));
    ASSERT_EQ(b, U"def");
  }
  {
    const JustStr a{U"ab"};
    std::u32string_view b{U"abc"};
    ASSERT_FALSE(a.Check(b));
    ASSERT_EQ(b, U"c");
  }
}