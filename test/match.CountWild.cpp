#include <gtest/gtest.h>
#include <match.cpp>

TEST(CountWild, check) {
  const CountWild a{2};
  {
    std::u32string_view b{U""};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"a"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"ab"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"abc"};
    ASSERT_FALSE(a.Check(b));
    ASSERT_EQ(b, U"c");
  }
  {
    std::u32string_view b{U"あ"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"あい"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"あいう"};
    ASSERT_FALSE(a.Check(b));
    ASSERT_EQ(b, U"う");
  }
}