#include <gtest/gtest.h>

#include <digger.cpp>

const fs::path ROOT{TESTDIG_ROOT_DIR};

TEST(Digger, not_exists) {
  std::error_code ec;
  gloob::Digger a{ROOT, "none", fs::directory_options::none, ec};
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_FALSE(a.Has());
}

TEST(Digger, jp) {
  std::error_code ec;
  gloob::Digger a{ROOT, "root.txt", fs::directory_options::none, ec};
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  a.Search(ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_FALSE(a.Has());
}

TEST(Digger, re_pt) {
  std::error_code ec;
  std::vector<fs::directory_entry> buf;
  gloob::Digger a{ROOT, "**/*.png", fs::directory_options::none, ec};
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  while (a.Has()) {
    buf.push_back(a.GetEntry());
    a.Search(ec);
    ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  }
  std::sort(std::begin(buf), std::end(buf));
  ASSERT_EQ(buf.size(), 9);
  ASSERT_EQ(ROOT / "a/1/1.png", buf[0]);
  ASSERT_EQ(ROOT / "a/3/3.png", buf[1]);
  ASSERT_EQ(ROOT / "a/5/5.png", buf[2]);
  ASSERT_EQ(ROOT / "b/1/1.png", buf[3]);
  ASSERT_EQ(ROOT / "b/3/3.png", buf[4]);
  ASSERT_EQ(ROOT / "b/5/5.png", buf[5]);
  ASSERT_EQ(ROOT / "c/1/1.png", buf[6]);
  ASSERT_EQ(ROOT / "c/3/3.png", buf[7]);
  ASSERT_EQ(ROOT / "c/5/5.png", buf[8]);
}

TEST(Digger, pt_jp) {
  std::error_code ec;
  std::vector<fs::directory_entry> buf;
  gloob::Digger a{ROOT, "[ac]/1/", fs::directory_options::none, ec};
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  while (a.Has()) {
    buf.push_back(a.GetEntry());
    a.Search(ec);
    ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  }
  std::sort(std::begin(buf), std::end(buf));
  ASSERT_EQ(buf.size(), 2);
  ASSERT_EQ(ROOT / "a/1", buf[0]);
  ASSERT_EQ(ROOT / "c/1", buf[1]);
}

TEST(Digger, jp_pt) {
  std::error_code ec;
  std::vector<fs::directory_entry> buf;
  gloob::Digger a{ROOT, "b/[2-4]/", fs::directory_options::none, ec};
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  while (a.Has()) {
    buf.push_back(a.GetEntry());
    a.Search(ec);
    ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  }
  ASSERT_EQ(buf.size(), 1);
  ASSERT_EQ(ROOT / "b/3", buf[0]);
}