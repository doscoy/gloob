readonly root="$(cd "$(dirname "${BASH_SOURCE[0]:-${(%):-%x}}")"; pwd)/dir"

mkdir -p $root
touch $root/root.txt

for d1 in "a" "b" "c"; do
  mkdir -p $root/$d1
  touch $root/$d1/$d1.txt
  for d2 in 1 3 5; do
    mkdir -p $root/$d1/$d2
    touch $root/$d1/$d2/$d2.png
    for d3 in "甲" "乙" "丙"; do
      mkdir -p $root/$d1/$d2/$d3
      touch $root/$d1/$d2/$d3/$d3.bin
    done
  done
done