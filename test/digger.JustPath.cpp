#include <gtest/gtest.h>

#include <digger.cpp>

const fs::path ROOT{TESTDIG_ROOT_DIR};

TEST(JustPath, rootfile) {
  JustPath a{"root.txt"};
  std::error_code ec;
  a.Init(fs::directory_entry{ROOT}, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  ASSERT_EQ(ROOT / "root.txt", a.GetEntry().path());
  ASSERT_FALSE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_FALSE(a.Has());
}

TEST(JustPath, deepfile) {
  JustPath a{"a/1/甲/甲.bin"};
  std::error_code ec;
  a.Init(fs::directory_entry{ROOT}, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  ASSERT_EQ(ROOT / "a/1/甲/甲.bin", a.GetEntry().path());
}

TEST(JustPath, not_exists) {
  JustPath a{"c/5/丁"};
  std::error_code ec;
  a.Init(fs::directory_entry{ROOT}, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_FALSE(a.Has());
}