#include <gtest/gtest.h>

#include <match.cpp>

// TEST(RepeatBrackets, a) {
//   const RepeatBrackets a{U"0-9"};
//   {
//     std::u32string_view b{U"059"};
//     ASSERT_TRUE(a.Check(b));
//   }
//   {
//     std::u32string_view b{U"059abc"};
//     ASSERT_FALSE(a.Check(b));
//     ASSERT_EQ(b, U"abc");
//   }
// }

// TEST(RepeatBrackets, a) {
//   const RepeatBrackets a{U"0-9a-z"};
//   {
//     std::u32string_view b{U"059"};
//     ASSERT_TRUE(a.Check(b));
//   }
//   {
//     std::u32string_view b{U"059abc"};
//     ASSERT_TRUE(a.Check(b));
//   }
// }