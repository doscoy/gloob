#include <gtest/gtest.h>

#include <match.cpp>

TEST(SingleBrackets, range) {
  const SingleBrackets a{U"0-9"};
  {
    std::u32string_view b{U"0"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"9"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"5"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"01"};
    ASSERT_FALSE(a.Check(b));
    ASSERT_EQ(b, U"1");
  }
  {
    std::u32string_view b{U"a"};
    ASSERT_FALSE(a.Check(b));
  }
}

TEST(SingleBrackets, single_exc) {
  const SingleBrackets a{U"!"};
  {
    std::u32string_view b{U"!"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"a"};
    ASSERT_FALSE(a.Check(b));
  }
}

TEST(SingleBrackets, double_exc) {
  const SingleBrackets a{U"!!"};
  {
    std::u32string_view b{U"!"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"a"};
    ASSERT_TRUE(a.Check(b));
  }
}

TEST(SingleBrackets, negative_range) {
  const SingleBrackets a{U"!a0-9"};
  {
    std::u32string_view b{U"0"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"9"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"5"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"a"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"b"};
    ASSERT_TRUE(a.Check(b));
  }
}

TEST(SingleBrackets, range_between_char) {
  const SingleBrackets a{U"a0-3c5-9e"};
  {
    std::u32string_view b{U"0"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"3"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"5"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"9"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"a"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"c"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"e"};
    ASSERT_TRUE(a.Check(b));
  }
  {
    std::u32string_view b{U"4"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"b"};
    ASSERT_FALSE(a.Check(b));
  }
  {
    std::u32string_view b{U"d"};
    ASSERT_FALSE(a.Check(b));
  }
}