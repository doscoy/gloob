#include <gtest/gtest.h>

#include <match.cpp>

TEST(CharRange, ascii) {
  CharRange a{U'0', U'9'};
  ASSERT_TRUE(a.Has(U'0'));
  ASSERT_TRUE(a.Has(U'5'));
  ASSERT_TRUE(a.Has(U'9'));
  ASSERT_FALSE(a.Has(U'a'));
  ASSERT_FALSE(a.Has(U'あ'));
}

TEST(CharRange, jp) {
  CharRange a{U'あ', U'お'};
  ASSERT_TRUE(a.Has(U'あ'));
  ASSERT_TRUE(a.Has(U'う'));
  ASSERT_TRUE(a.Has(U'お'));
  ASSERT_FALSE(a.Has(U'a'));
  ASSERT_FALSE(a.Has(U'ん'));
}