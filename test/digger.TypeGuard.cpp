#include <gtest/gtest.h>

#include <digger.cpp>

const fs::path ROOT{TESTDIG_ROOT_DIR};

TEST(TypeGuard, dir_only) {
  TypeGuard a{true};
  std::error_code ec;
  fs::directory_entry entry;

  entry = fs::directory_entry{ROOT};
  a.Init(entry, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  ASSERT_EQ(ROOT, a.GetEntry().path());
  ASSERT_FALSE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();

  entry = fs::directory_entry{ROOT / "root.txt"};
  a.Init(entry, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_FALSE(a.Has());
}

TEST(TypeGuard, not_dir_only) {
  TypeGuard a{false};
  std::error_code ec;
  fs::directory_entry entry;

  entry = fs::directory_entry{ROOT / "root.txt"};
  a.Init(entry, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_TRUE(a.Has());
  ASSERT_EQ(ROOT / "root.txt", a.GetEntry().path());
  ASSERT_FALSE(a.Search(ec));
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();

  entry = fs::directory_entry{ROOT};
  a.Init(entry, ec);
  ASSERT_FALSE(ec) << ec.value() << ": " << ec.message();
  ASSERT_FALSE(a.Has());
}
