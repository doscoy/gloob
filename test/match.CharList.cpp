#include <gtest/gtest.h>
#include <match.cpp>

TEST(CharList, check) {
  CharList a{U"abcあいう😄"};
  ASSERT_TRUE(a.Has(U'a'));
  ASSERT_TRUE(a.Has(U'あ'));
  ASSERT_TRUE(a.Has(U'😄'));
  ASSERT_FALSE(a.Has(U'x'));
  ASSERT_FALSE(a.Has(U'ア'));
  ASSERT_FALSE(a.Has(U'😃'));
}