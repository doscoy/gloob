#include <gtest/gtest.h>
#include <match.cpp>

TEST(FindRBracket, check) {
  ASSERT_EQ(FindRBracket(U"0123456789"), std::u32string_view::npos);
  ASSERT_EQ(FindRBracket(U"[]"), std::u32string_view::npos);
  ASSERT_EQ(FindRBracket(U"[123]"), 4);
  ASSERT_EQ(FindRBracket(U"[]23]"), 4);
  ASSERT_EQ(FindRBracket(U"[!]3]"), 2);
}